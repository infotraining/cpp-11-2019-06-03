#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct IGadget
{
	virtual ~IGadget() = default;
	virtual void info() const = 0;
};

struct Gadget : IGadget
{
	int id{};
	string name = "(not set)";

	Gadget() = default;

	Gadget(string name) : Gadget{ 0, name } // delegating construction
	{
		static int gen_id;
		id = ++gen_id;
	}

	Gadget(int id, const string& name)
		: id{ id }, name{ name }
	{
	}

	Gadget(const Gadget&) = delete;
	Gadget& operator=(const Gadget&) = delete;

	void info() const override
	{
		cout << "Gadget(" << id << ", " << name << ")\n";
	}
};

struct SuperGadget : Gadget
{
	using Gadget::Gadget; // inheritance of constructor

	SuperGadget(string) = delete;

	void info() const override
	{
		cout << "SuperGadget(" << id << ", " << name << ")\n";
	}
};

template <typename T, typename = enable_if_t<is_same_v<T, int>>>
void foo(T value)
{
	cout << "foo(value: " << value << ")\n";
}

void foo(double) = delete;

TEST_CASE("classes")
{
	Gadget g1{ "watch" };

	IGadget* ptr_g = &g1;
	ptr_g->info();

	//Gadget g2 = g1;

	foo(42);
	//foo(3.14);

	short x = 42;
	//foo(x);

	auto value = 3.14f;
	//foo(value);

	SuperGadget sg1{ 52, "ipad" };
	ptr_g = &sg1;
	ptr_g->info();
}

struct Data
{
	int v;
};

TEST_CASE("")
{
	auto p1 = new Data;

	cout << p1->v << endl;

	auto p2 = new Data{};

	cout << p2->v << endl;
}