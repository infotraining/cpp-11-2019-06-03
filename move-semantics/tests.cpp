#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

string full_name(const string& fn, const string& ln)
{
	return fn + " " + ln;
}

TEST_CASE("r-value refs")
{
	string first = "Jan";

	vector<string> names;

	SECTION("C++98")
	{		
		string& ref_name = first;

		const string& cref_name = full_name(first, "Kowalski");
		//cref_name[0] = 'P';
	}

	SECTION("C++11")
	{
		string&& rvref_name = full_name(first, "Kowalski");

		rvref_name[0] = 'P';
		cout << rvref_name << endl;

		names.push_back(std::move(rvref_name));
		names.push_back(full_name(first, "Nowak"));

		// string&& rvref_fname = first; // ERROR - ill-formed
	}
}

TEST_CASE("moving vector")
{
	vector<int> v1 = { 1, 2, 3, 4, 5 };
	
	vector<int> v2 = std::move(v1);

	REQUIRE(v1.size() == 0); // UB - don't touch moved object

	v1 = vector<int>{ 6, 7, 8 }; // unless is reassingned
}

TEST_CASE("moving primitives")
{
	int x = 10;
	int* ptr = &x;

	int mx = std::move(x); // copy
	int* mptr = std::move(ptr); // copy
}

struct Data
{
	string name;	
	vector<int> data;

	Data(string name, vector<int> data) : name{ std::move(name) }, data(std::move(data))
	{}
};


TEST_CASE("constructing with move")
{
	string lvalue = "data set 1";
	vector<int> dataset = { 1, 2, 3 };

	Data d1{ lvalue , dataset};

	Data d2{ "data set 2", {1, 2, 3} };
}

class Array
{
	int* buffer_;
	size_t size_;
	string name_;

public:
	using iterator = int*;
	using const_iterator = const int*;

	Array(std::initializer_list<int> il, string name)
		: buffer_{new int[il.size()]}, size_{il.size()}, name_{std::move(name)}
	{
		assert(name_ != ""s);

		std::copy(std::begin(il), std::end(il), buffer_);

		cout << "Array(name: " << name_ << ", { ";
		for (const auto& item : *this)
		{
			cout << item << " ";
		}
		cout << ")\n";
	}

	Array(const Array& other)
		: buffer_{ new int[other.size_] }, size_{ other.size_ }, name_{ other.name_ }
	{
		std::copy(other.buffer_, other.buffer_ + size_, buffer_);

		cout << "Array(copy ctor: " << name_ << ")\n";
	}

	Array(Array&& other) noexcept // move constructor
		: buffer_{other.buffer_}, size_{other.size_}, name_{std::move(other.name_)}
	{
		other.buffer_ = nullptr;

		cout << "Array(move ctor: " << name_ << ")\n";
	}

	Array& operator=(const Array& other)
	{
		if (this != &other)
		{
			name_ = other.name_;
			int* temp_buffer = new int[other.size_];
			delete[] buffer_;

			buffer_ = temp_buffer;
			size_ = other.size_;
			std::copy(other.buffer_, other.buffer_ + size_, buffer_);
		}

		cout << "Array(copy =: " << name_ << ")\n";

		return *this;
	}

	Array& operator=(Array&& other)
	{
		if (this != &other)
		{
			delete[] buffer_;

			buffer_ = other.buffer_;
			other.buffer_ = nullptr;
			size_ = other.size_;
			name_ = std::move(other.name_);
		}

		cout << "Array(move =: " << name_ << ")\n";

		return *this;
	}

	~Array()
	{
		delete[] buffer_;

		cout << "~Array(name: " << (name_.empty() ? "after move" : name_) << ")\n";
	}

	size_t size() const
	{
		return size_;
	}

	int& operator[](size_t index)
	{
		return buffer_[index];
	}

	iterator begin()
	{
		return buffer_;
	}

	iterator end()
	{
		return buffer_ + size_;
	}

	const_iterator begin() const
	{
		return buffer_;
	}

	const_iterator end() const
	{
		return buffer_ + size_;
	}

	string name() const
	{
		return name_;
	}
};

Array create_array()
{
	static int id = 0;

	return Array{ {1, 2, 3}, to_string(++id) };
}

void use(const Array& arr)
{
	cout << arr.name() << " - ";
	for (const auto& item : arr)
		cout << item << " ";
	cout << endl;
}

TEST_CASE("Array")
{
	Array arr1{ {1, 2, 3, 4}, "ds1" };

	for (const auto& item : arr1)
		cout << item << " ";
	cout << endl;

	Array arr2 = std::move(arr1);

	cout << "\n-------\n";

	Array arr3 = create_array(); // RVO - copy elision

	for (const auto& item : arr3)
		cout << item << " ";
	cout << endl;

	use(arr3);

	use(create_array());
}

struct BigData
{
	Array dataset1;
	vector<int> dataset2;
};

BigData load_from_file(string name = "dataset")
{
	return { {{1, 2, 3, 4}, std::move(name)}, {5, 6, 7} };
}

TEST_CASE("coping & moving")
{
	cout << "\n\n-------------\n";

	Array arr1 = create_array();
	Array arr2 = create_array();
	Array arr3 = arr1;
	Array arr4 = move(arr2);
}

TEST_CASE("big data")
{
	cout << "\n\n-------------\n";

	BigData b1{ {{1, 2, 3, 4}, "ds1"}, {5, 6, 7} };

	BigData b2 = b1; // copy

	BigData b3 = std::move(b1); // move

	b3.dataset2[0] = 1;

	BigData from_file = load_from_file(); // implicit move or RVO

	vector<BigData> datasets;
	datasets.push_back(move(from_file)); // move to container
	datasets.push_back(load_from_file("new dataset")); // implicit move to container
}

template <typename T, typename Arg1, typename Arg2>
unique_ptr<T> my_make_unique(Arg1&& arg1, Arg2&& arg2)
{
	return unique_ptr<T>(new T(std::forward<Arg1>(arg1), std::forward<Arg2>(arg2)));
}

TEST_CASE("perfect forwarding")
{
	auto il = { 1, 2, 3 };
	std::unique_ptr<Array> arr = my_make_unique<Array>(il, "unique_ptr");
	 
	REQUIRE(arr->size() == 3);

	cout << "\n\n============\n";

	vector<Array> vec;
	
	vec.push_back(Array({ 1, 2, 3 }, "arr1"));
	cout << "---------\n";
	
	vec.emplace_back(il, "arr2");		
	cout << "---------\n";

	for (int i = 0; i < 10; ++i)
	{
		vec.push_back(create_array());
		cout << "---------\n";
	}
}