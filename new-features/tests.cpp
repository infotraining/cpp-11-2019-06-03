#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <string_view>
#include <vector>
#include <map>
#include <type_traits>
#include <array>

#include "catch.hpp"

using namespace std;

TEST_CASE("raw strings")
{
	string path1 = "c:\\nasz katalog\\backup";
	string path2 = R"(c:\nasz katalog\backup - "cytat")";
	string text = R"autodesk("tekst(1, 2, 3)")autodesk"; // custom delimiter autodesk()autodesk

	cout << path1 << endl;
	cout << path2 << endl;
	cout << text << endl;

	string multi_line = R"(Line1
  Line2
Line3)"
"\nLine4\nLine5";

	cout << multi_line << endl;
}

TEST_CASE("string literal")
{
	auto text = "text"s; // string

	REQUIRE(text == "text"s);

	auto raw_text = R"(\n\n\n)"s; // string
}

TEST_CASE("range based for")
{
	SECTION("works with containers")
	{
		vector<string> vec = { "1", "2", "3" };

		for (auto& item : vec)
		{
			cout << item << " ";
			item = "text";
		}
		cout << "\n";

		SECTION("is interpeted as")
		{
			auto&& range = vec;

			for (auto it = vec.begin(); it != vec.end(); ++it)
			{
				auto& item = *it; // string is not copied

				cout << item << " ";
				cout << "\n";
			}
		}
	}

	SECTION("works with native arrays")
	{
		int tab[] = { 1, 2, 3 };

		for (const auto& item : tab)
		{
			cout << item << " ";
		}
		cout << "\n";


		SECTION("is interpeted as")
		{
			for (auto it = begin(tab); it != end(tab); ++it)
			{
				const auto& item = *it; // string is not copied

				cout << item << " ";
				cout << "\n";
			}
		}
	}

	SECTION("works with initializer_list")
	{
		for (const auto& item : { 1, 2, 3, 4 })
		{
			cout << item << " ";
		}
		cout << "\n";
	}

	SECTION("works with c-string literal")
	{
		SECTION("with '\0' at the end")
		{
			for (const auto& c : "text")
			{
				cout << c << ".";
			}
			cout << "\n";
		}

		SECTION("without '\0' at the end - since C++17")
		{
			for (auto& c : "text"sv)
			{
				cout << c << ".";
			}
			cout << "\n";
		}
	}
}

string_view process_text(string_view text)
{
	cout << "processing text " << text << "...\n";

	// AnsiC API - not compatible

	return text; // dangerous
}

string full_name(const string& fn, const string& ln)
{
	return fn + " " + ln;
}

TEST_CASE("string view")
{
	string_view sv1 = "text";

	string txt = "abc"s;
	string_view sv2 = txt;

	SECTION("Beware - UB")
	{
		// string_view dangling_text = full_name("Adam", "Nowak"); // UB
	}

	process_text("ABC");
	process_text(txt);

	SECTION("Beware - UB (temporary object)")
	{
		// auto result = process_text(full_name("Jan", "Kowalski"));
		// cout << result << "\n";
	}
}

enum DaysOfWeek : uint8_t; // forward declaration of enum

enum DaysOfWeek : uint8_t { Mon, Tue, Wed, Thd, Fri, Sat, Sun };

TEST_CASE("enums")
{
	DaysOfWeek day = Mon;
	day = static_cast<DaysOfWeek>(4);

	uint8_t index = day;
}

enum class Coffee { espresso, latte, capuccino };

TEST_CASE("scoped enumeration")
{
	Coffee c = Coffee::capuccino;
	Coffee c2 = static_cast<Coffee>(2);

	REQUIRE(c2 == Coffee::capuccino);

	SECTION("implicit conversion to integral type is illegal")
	{
		int index = static_cast<int>(c);

		REQUIRE(index == 2);
	}
}

template<typename E>
struct enable_bitmask_operators : std::false_type
{
	static constexpr bool enable = false;
};

template<typename E>
typename std::enable_if<enable_bitmask_operators<E>::enable, E>::type
operator |(E lhs, E rhs)
{
	typedef typename std::underlying_type<E>::type underlying;
	return static_cast<E>(
		static_cast<underlying>(lhs) | static_cast<underlying>(rhs));
}

enum class my_bitmask
{
	first = 1, second = 2, third = 4
};

template<>
struct enable_bitmask_operators<my_bitmask>
{
	static constexpr bool enable = true;
};

unsigned long long generate_id()
{
	static unsigned long long  seed = 0;
	return ++seed;
}

int different_generator()
{
	return 42;
}

struct PointAggregate
{
	int x, y;
};

struct Point
{
	int x, y;

	Point(int x = 0, int y = 0) : x{ x }, y{ y }
	{}
};

TEST_CASE("uniform init syntax")
{
	int x1{ 42 };
	int& x2{ x1 };

	unsigned long long value{ generate_id() };

	int tab[] = { 1, 2, 3, 4 };

	PointAggregate pt1{ 1, 2 };
	PointAggregate pt2{};
	PointAggregate pt3{ 1 };

	//REQUIRE(pt2.x == 0 && pt2.y == 0);

	Point pt{ 1, 2 };
	Point origin{};

	vector<int> vec = { 1, 2, 3, 4 };
	list<int> data{ 1, 2, 3, 4 };
	const map<int, string> dict = { { 1, "one"}, { 2, "two" }, { 3, "three" } };
}

struct Container
{
	using iterator = vector<int>::iterator;
	using const_iterator = vector<int>::const_iterator;

	std::vector<int> data;

	Container() {}

	Container(int size, int value)
		: data(size, value)
	{}

	Container(std::initializer_list<int> lst)
		: data{ lst }
	{
	}

	iterator begin()
	{
		return data.begin();
	}

	iterator end()
	{
		return data.end();
	}
};

TEST_CASE("Container initialized with list")
{
	Container default_container; // default constructor
	Container cont = { 1, 2, 3, 4 }; // constructor with il

	vector<int> vec = { 1, 2, 3 };
	vec.insert(vec.end(), { 4, 5, 6 });

	vector<int> vec2(10, -1);
	vector<int> vec3{ 10, -1 };
}

template <typename T>
using StrKeyMap = map<string, T>;

TEST_CASE("alias template")
{
	StrKeyMap<int> dict = { {"one", 1}, {"two", 2} };
}

namespace Case1
{
	template <typename T>
	struct my_template
	{
		static const int type; // type is a static member
	};
}

namespace Case2
{
	template <typename T>
	struct my_template
	{
		typedef int type; // type is nested type
	};
}

namespace AliasForTraits
{
	template <typename T>
	using remove_const_t = typename remove_const<T>::type; // typename keyword required - remove_const is name dependent on template argument
}

template <typename T>
void foo(T& arg)
{
	remove_const_t<T> item{};

	item = arg + 1;

	puts(__FUNCSIG__);
}

TEST_CASE("using type traits")
{
	const int x = 10;
	foo(x);
}

struct Date
{	
	int year;
	string month;
	int day;
};

Date get_date()
{
	return { 2019, "Jun", 4 };
}

TEST_CASE("structured binding")
{
	const auto& [y, m, d] = get_date();

	SECTION("works like this")
	{
		const auto& unnamed_obj = get_date();
		auto& y = unnamed_obj.year;
		auto& m = unnamed_obj.month;
		auto& d = unnamed_obj.year;
	}	

	cout << y << " " << m << " " << d << endl;
}

constexpr int factorial(int n)
{
	if (n == 1)
		return 1;
	else
		return factorial(n - 1) * n;
}

template <typename Iterator, typename T>
constexpr Iterator my_find(Iterator first, Iterator last, const T& item)
{
	while (first != last)
	{
		if (*first == item)
			return first;
		++first;
	}

	return last;
}

TEST_CASE("constexpr")
{
	static_assert(factorial(3) == 6);

	constexpr int tab[factorial(3)] = { 1, 87, 42, 12 };

	static_assert(my_find(begin(tab), end(tab), 42) != end(tab));
}

TEST_CASE("explicit lambda constexpr")
{
	constexpr array<int, factorial(3)> values = { 1, 2 };

	auto sum = [](const auto& data) constexpr {
		long result{};
		for (auto it = begin(data); it != end(data); ++it)
			result += *it;
		return result;
	};

	static_assert(sum(values) == 3);
}

template <typename... Args>
void variadic_function(Args... args)
{
	cout << sizeof...(args) << " args" << endl;
}

namespace Cpp11
{
	void variadic_print()
	{
		cout << "\n";
	}

	template <typename Head, typename... Tail>
	void variadic_print(const Head& h, const Tail& ... tail)
	{
		cout << h << " ";
		variadic_print(tail...);
	}
}

namespace Cpp17
{
	template <typename Head, typename... Tail>
	void variadic_print(const Head& h, const Tail& ... tail)
	{
		cout << h << " ";
		if constexpr (sizeof...(tail) > 0)
			variadic_print(tail...);
		else
			cout << "\n";
	}
}


TEST_CASE("variadic templates")
{
	variadic_function(1, 2, 3, 4);
	variadic_function(1, 2, 3, 4, 5, 6, 7);
	variadic_function();

	Cpp17::variadic_print(1, 2, 'c', "string");
}