#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

struct X
{
	int  value;

	X(int v, const string& name) :
		value{v}
	{
		cout << "X(" << value << ", " << name << ")\n";
	}

	X()
	{
		static int gen_id = 0;
		value = ++gen_id;

		cout << "X(" << value << ")\n";
	}

	X(const X&) = delete;
	X(X&&) = delete;
	X& operator=(const X&) = delete;
	X& operator=(X&&) = delete;

	~X()
	{
		cout << "~X(" << value << ")\n";
	}
};

namespace Legacy
{
	X* get_x()
	{
		return new X();
	}

	void destroy(X* ptr)
	{
		// use of ptr
	
		delete ptr;
	}

	void foo()
	{
		X* ptr = get_x();

		delete ptr;
	}

	void use(X* ptr)
	{
		cout << "using " << ptr->value << endl;

		delete ptr;
	}

	//void leaky_code()
	//{
	//	get_x(); // leak

	//	X x;
	//	use(&x); // segfault
	//}

	int* create_table()
	{
		return new int[1024];
	}
}

unique_ptr<X> get_x()
{
	return make_unique<X>();
}

void use(X* ptr)
{
	if (ptr)
		cout << "using " << ptr->value << endl;
}

unique_ptr<X> use(unique_ptr<X> ptr)
{
	if (ptr)
		cout << "using " << ptr->value << endl;

	return ptr;
}

void safe_code()
{
	get_x(); // no_leak

	unique_ptr<X> ptr = get_x(); // implicit move

	// auto backup_ptr = ptr; // unique_ptr is non-copyable

	auto ptr2 = std::move(ptr);

	cout << ptr2->value << "\n";

	use(ptr2.get());

	auto ptr3 = use(std::move(ptr2));

	cout << "--- After use..." << endl;

	vector<unique_ptr<X>> vec_ptrs;
	vec_ptrs.push_back(get_x());
	vec_ptrs.push_back(get_x());
	vec_ptrs.push_back(get_x());
	vec_ptrs.push_back(std::move(ptr3));


	for (const auto& p : vec_ptrs)
		cout << p->value << " ";
	cout << "\n";

	vec_ptrs.clear();

	cout << "--- After clear..." << endl;
}

TEST_CASE("smart pointers")
{
	safe_code();
}

void* create_buffer()
{
	return malloc(1024);
}

TEST_CASE("custom deallocator")
{
	cout << "\n\n-----------Custom deallocators\n";

	//SECTION("legacy")
	{
		FILE* f = fopen("data.txt", "w");

		// write to file
		//fprintf(f, "%d", 42);

		if (f)
			fclose(f);
	}

	//SECTION("modern c++")
	{
		unique_ptr<FILE, int(*)(FILE*)> f{ fopen("data.txt", "w"), &fclose };

		shared_ptr<FILE> shared_f{ fopen("data2.txt", "w"), [](FILE* f) { if (f) fclose(f); } }; // may cause UB

		//{
		//	shared_ptr<void*> guard(0, [](void*) { cout << "Cleanup at scope exit"; });

		//	//..
		//}

		//fprintf(f.get(), "%d", 42);

		auto free_dealloc = [](void* ptr) { free(ptr); };
		unique_ptr<void, decltype(free_dealloc)> buffer{ create_buffer(), free_dealloc };

		cout << "buffer at: " << buffer.get() << "\n";
	}

	{
		unique_ptr<int[]> table{ Legacy::create_table() };

		for (int i = 0; i < 1024; i++)
		{
			table[i] = 0;
		}

	} // call to delete[]

	{
		unique_ptr<X> ptr(Legacy::get_x());

		cout << ptr->value << endl;

		Legacy::destroy(ptr.release());
	}
}

TEST_CASE("shared_ptr with unique_ptr")
{
	unique_ptr<X> uptr = make_unique<X>();

	shared_ptr<X> sptr = std::move(uptr);
}