#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

class ClosureClass_2345165
{
public:
	auto operator()() const { cout << "lambda!\n"; }
};

class GenericClosureClass_237468
{
public:
	template <typename T1, typename T2>
	auto operator()(const T1& a, const T2& b) const { return a + b; }
};

TEST_CASE("lambda expression")
{
	auto l1 = []() { cout << "lambda!\n"; };
	l1();

	ClosureClass_2345165 lc = ClosureClass_2345165{};
	lc();

	auto multiply = [](int a, int b) { return a * b; };

	REQUIRE(multiply(3, 5) == 15);

	SECTION("since C++14")
	{
		auto add = [](const auto& a, const auto& b) { return a + b; };

		REQUIRE(add(1, 2) == 3);
		REQUIRE(add(3.14, 3.14) == Approx(6.28));
		REQUIRE(add("abc"s, "cde"s) == "abccde");
	}

	vector<shared_ptr<int>> vec = { make_shared<int>(3), make_shared<int>(6), make_shared<int>(1) };
	sort(begin(vec), end(vec), [](const auto& a, const auto& b) { return *a < *b; });

	for (const auto& p : vec)
	{
		cout << *p << " ";
	}
	cout << endl;
}

TEST_CASE("capture & closures")
{
	vector<int> vec = { 1, 2, 3 };
	int count = 0;

	for_each(begin(vec), end(vec), [&count](int x) { ++count; });
	REQUIRE(count == 3);

	int factor = 10;
	auto fx = [factor](int x) { return x * factor; };
	
	factor = 100;
	transform(begin(vec), end(vec), begin(vec), fx);

	for_each(begin(vec), end(vec), [](int x) { cout << x << " "; });
	cout << "\n";
}

class MutableClosureClass
{
	int captured_x;

public:
	auto operator()() 
	{
		++captured_x; return captured_x;
	}
};

TEST_CASE("lambdas are immutable by default")
{
	int x = 10;

	auto l = [x]() mutable { ++x; return x; };

	REQUIRE(l() == 11);
	REQUIRE(l() == 12);
	REQUIRE(l() == 13);

	vector<int> vec(10);
	generate_n(begin(vec), 10, l);
	for_each(begin(vec), end(vec), [](int x) { cout << x << " "; });
	cout << "\n";
}

auto create_generator(int seed, int step = 1)
{
	return [seed, step]() mutable { seed += step; return seed; };
}

TEST_CASE("return lambda from function")
{
	vector<int> vec(10);
	generate_n(begin(vec), 10, create_generator(0, 2));
	for_each(begin(vec), end(vec), [](int x) { cout << x << " "; });
	cout << "\n";
}

template <typename F>
void foo(F f)
{
	f();
}

TEST_CASE("passing lambda to funtion")
{
	foo([]() { cout << "hello from foo\n"; });
}

TEST_CASE("storing lambdas")
{
	SECTION("auto")
	{
		int x = 10;
		static auto l = [x]() { return x; };

		REQUIRE(l() == 10);
	}

	SECTION("function ptr for non-capturing lambda expression")
	{
		int x = 10;
		int (*ptr)() = [] { return 10; };

		REQUIRE(ptr() == 10);
	}

	SECTION("std::function")
	{
		int x = 10;
		std::function<int()> f = [x]() { return x; };

		REQUIRE(f() == 10);
	}
}

int pseudo_local = 10;

struct X
{
	int value;

	void do_sth()
	{
		int x;
		auto l = [this, x] { cout << value << " " << pseudo_local << "\n"; };

		pseudo_local = 20;

		l();
	}

	~X()
	{
		cout << "~X(" << value << ")\n";
	}
};

auto create()
{
	unique_ptr<X> ptr = make_unique<X>(X{ 13 });

	return [captured_ptr = std::move(ptr)]{ cout << captured_ptr->value << "\n"; };	
}

TEST_CASE("capture expressions - since C++14")
{
	{
		auto l = create();
		l();
	}
	cout << "---- END of scope\n";
}