# Programming in C++ 11

## Docs ##

* https://infotraining.bitbucket.io/cpp-11/

## Links ##

* http://isocpp.github.io/CppCoreGuidelines/CppCoreGuidelines
* https://devblogs.microsoft.com/cppblog/two-phase-name-lookup-support-comes-to-msvc/

## Ankieta ##

* https://docs.google.com/forms/d/e/1FAIpQLSdJqLRWFIZsUnwBcoO84fn-dq7q4FjZXxhOuCqoMWInLZ8HLA/viewform?hl=pl