#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>
#include <list>
#include <map>

#include "catch.hpp"

using namespace std;

TEST_CASE("auto type deduction")
{
	auto i = 42; // int

	int data[] = { 1, 2, 3 };

	for (auto it = cbegin(data); it != cend(data); ++it)
	{
		cout << *it << " ";
		// *it = 42; // ERROR - read only access with iterator
	}
	cout << "\n";
}

template <typename Arg>
void deduce1(Arg arg)
{
	puts(__FUNCSIG__);
}

template <typename Arg>
void deduce2(Arg& arg)
{
	puts(__FUNCSIG__);
}

template <typename Arg>
void deduce3(Arg&& arg)
{
	puts(__FUNCSIG__);
}

namespace gcc_extension
{
	//void deduce1(auto arg)
	//{
	//	//...
	//}
}

namespace Cpp20
{
	//void sort(RandomAccessIterator auto first, RandomAccessIterator auto last)
	//{
	//  //...
	//}
}

void foo(int)
{}

TEST_CASE("auto type deduction - description")
{
	SECTION("Case 1 - const, volatile & refs are removed")
	{
		int x = 42;
		auto ax = x;
		deduce1(x);

		const int& cref_x = x;
		auto ax2 = cref_x;
		deduce1(cref_x);

		const int cx = 42;
		auto ax3 = cx;
		deduce1(cx);

		SECTION("arrays decay to pointer")
		{
			int tab[10];
			auto ax4 = tab; // int*
			deduce1(ax4);
		}

		SECTION("function decay to pointer")
		{
			auto ax5 = foo;
			deduce1(foo);
		}

		cout << "\n\n";
	}

	SECTION("Case 2 - const & volatile are preserved")
	{

		int x = 42;
		int& ref_x = x;
		const int& cref_x = x;

		auto& aref1 = x;
		deduce2(x);

		auto& aref2 = ref_x;
		deduce2(ref_x);
		
		auto& aref3 = cref_x;
		deduce2(cref_x);

		SECTION("arrays do not decay to pointer")
		{
			int tab[10];
			auto& ax4 = tab; // int(&)[10]
			deduce2(tab);
		}

		SECTION("function do not decay to pointer")
		{
			auto& ax5 = foo;
			deduce2(foo);
		}
	}
}

TEST_CASE("universal reference")
{
	int x = 10;
	deduce3(x);
	auto&& ax = x; // int&

	deduce3(10);
	auto&& ay = 10;
}

//TEST_CASE("special case")
//{
//	auto list = { 1, 2, 3 }; // std::initializer_list<int>
//	// deduce1({ 1, 2, 3 }); // ill-formed
//}
//
//TEST_CASE("direct initialization")
//{
//	auto i1 = 42;
//	auto i2(42);  // int i(42);
//
//	SECTION("Before C++17")
//	{
//		auto x{ 42 }; // auto x = {42}; // std::initializer_list<int>
//
//		//auto accepted{ 1, 2, 3 }; // std::initializer_list<int>
//	}
//
//	SECTION("Since C++17")
//	{
//		auto x{ 42 }; // int
//
//		//auto illegal{ 42, 42, 42 }; // ill-formed - only one item in braces allowed 
//
//		auto list = { 1, 2, 3 }; // OK
//	}
//}
//
//bool less_comp(int a, int b)
//{
//	return a < b;
//}
//
//
//TEST_CASE("decltype")
//{
//	map<int, string, bool (*)(int, int)> dict{ { {1, "one"}, {2, "two"}, {3, "three"} }, &less_comp };
//
//	for (map<int, string>::const_iterator it = dict.begin(); it != dict.end(); ++it)
//		cout << it->first << " : " << it->second << endl;
//	cout << endl;
//
//	auto backup = dict; // copy construction
//
//	auto greater_comp = [](int a, int b) { return a > b; };
//
//	decltype(dict) other_dict(begin(dict), end(dict), greater_comp);
//
//	map<int, string, decltype(greater_comp)> more_efficient_other_dict(begin(dict), end(dict), greater_comp);
//
//	for (const auto& [key, value] : other_dict)
//	{
//		cout << key << " : " << value << endl;
//	}
//	cout << endl;
//		
//	if (const auto & [pos, was_inserted] = dict.insert(pair(4, "four")); was_inserted)
//		cout << "item was inserted" << endl;
//}
//
//template <typename T>
//auto multiply(const T& a, const T& b) //-> decltype(a * b)
//{
//	return a * b;
//}
//
//auto get_decription(int value)
//{
//	if (value == 1)
//		return "one"s;
//
//	return "unknown"s;
//}
//
//decltype(auto) get_value(map<int, string>& dict, int key)
//{
//	return dict.at(key);
//}
//
//TEST_CASE("get_value")
//{
//	map<int, string> dict = { {1, "one"} };
//
//	get_value(dict, 1) = "jeden";
//
//	REQUIRE(dict.at(1) == "jeden");
//
//	int x = 42;
//	auto ax = (x);
//	decltype(auto) rx = (x);
//}
//
