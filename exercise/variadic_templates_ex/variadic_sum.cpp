#define CATCH_CONFIG_MAIN

#include "catch.hpp"
#include <cassert>
#include <iostream>
#include <type_traits>

using namespace std;

namespace vt
{
	namespace Cpp11
	{
		template <typename T>
		auto sum(T arg)
		{
			return arg;
		}

		template <typename Head, typename... Tail>
		auto sum(Head h, Tail... tail)
		{
			return h + sum(tail...);
		}
	}

	inline namespace Cpp17
	{
		template <typename... Args>
		common_type_t<Args...> sum(Args... args)
		{
			using CT = common_type_t<Args...>;
			return (... + static_cast<CT>(args)); // fold expression
		}
	}
}

TEST_CASE("variadic sum")
{
    SECTION("for ints")
    {
        auto sum = vt::sum(1, 3, 3);

        REQUIRE(sum == 7);
        static_assert(is_same<int, decltype(sum)>::value, "Error");
    }

    SECTION("for floating points")
    {
        auto dbl_sum = vt::sum(1.1, 3.0f, 3);

        REQUIRE(dbl_sum == Approx(7.1));
        static_assert(is_same<double, decltype(dbl_sum)>::value, "Error");
    }

    SECTION("for strings")
    {
        auto text = vt::sum("Hello", "#", "world", "!");

        REQUIRE(text == "Hello#world!");
        static_assert(is_same<string, decltype(text)>::value, "Error");
    }
}