#include <algorithm>
#include <numeric>
#include <iostream>
#include <string>
#include <vector>

#include "catch.hpp"

using namespace std;

void foo(int* ptr)
{
	if (ptr != nullptr)
		cout << "foo(int*: " << *ptr << ")\n";
	else
		cout << "foo(int*: nullptr)\n";
}

void foo(int value)
{
	cout << "foo(int: " << value << ")\n";
}

void foo(nullptr_t) = delete;
//{
//	cout << "foo(nullptr_t)\n";
//}

TEST_CASE("nullptr")
{
	int x = 10;

	foo(&x);
	//foo(NULL);

	//foo(nullptr);

	int* ptr = nullptr;
	foo(ptr);
}